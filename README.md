# Weather Today

Weather Today is the fast, light and nice app for looking weather right now.
You can use it in light or dark mode if you want.

[View on App Store](https://apps.apple.com/us/app/id1530173675)
