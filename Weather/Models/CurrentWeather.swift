//
//  CurrentWeather.swift
//  Weather
//
//  Created by Konstantin Filobok on 30.08.2020.
//  Copyright © 2020 Konstantin Filobok. All rights reserved.
//

import Foundation

struct CurrentWeather {
    let cityName: String
    
    let temperature: Double
    var temperatureString: String {
        return String(format: "%.0f", temperature)
    }
    
    let feelsLikeTemperature: Double
    var feelsLikeTemperatureString: String {
        return String(format: "%.0f", feelsLikeTemperature)
    }
    
    let conditionCode: Int
    var systemIconNameString: String {
        switch conditionCode {
        case 200...232: return "boltRain"
        case 300...321: return "drizzle"
        case 500...531: return "rain"
        case 600...622: return "snow"
        case 701...781: return "smog"
        case 800: return "sun"
        case 801...804: return "cloud"
        default: return "nosign"
        }
    }
    
    init?(currentWeatherData: CurrentWeatherData) {
        cityName = currentWeatherData.name
        temperature = currentWeatherData.main.temp
        feelsLikeTemperature = currentWeatherData.main.feelsLike
        conditionCode = currentWeatherData.weather.first!.id
    }
}
